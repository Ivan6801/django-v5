from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

from project.models import Car


def my_view(request):
    car_list = [
        {"title": "BMW"},
        {"title": "Mazda"},
        {"title": "Audi"}
    ]
    print(car_list)
    context = {
        "car_list": car_list
    }
    return render(request, "car_list.html", context)


def my_test_view(request, *args, **kwargs):
    print("args", args)
    print("kwargs", kwargs)
    return HttpResponse("Hello, world!")


class CarListView(TemplateView):
    template_name = "car_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cars'] = Car.objects.all()
        return context
