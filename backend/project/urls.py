from django.http import HttpResponse
from django.urls import path
from project.views import my_test_view


urlpatterns = [
    path('listado', my_test_view),
    path('detalle/<int:id>', my_test_view),
    path('marcas/<str:brand>', my_test_view),
]
